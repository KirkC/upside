= The load host power cycle =
by Zygo Blaxell <mailtoo@furryterror.org>

== The power cycle ==

This is a typical modern PC desktop workstation's power cycle, from AC
on to AC off and back again. I'll present some scenarios we want to
avoid by using a UPS, and their impact on UPS requirements when the
UPS and host are unattended.

1. AC input goes from 0V to nominal voltage (quickly--10ms?)

2. Host DC power supplies detect AC voltage change and come up. In
some BIOSes this is "AC Power Loss Restart" and can be enabled or
disabled. Since the load host is unattended, we assume this feature is
enabled. CPU boots and starts reading BIOS (or equivalent firmware)
code.

3. BIOS code reads non-volatile configuration storage (colloquially
known as CMOS).

4. BIOS code checks and sets an error flag in non-volatile configuration storage.

5. BIOS code finishes platform initialization according to stored configuration.

6. BIOS code clears the error flag set in step 4.

7. BIOS code scans for storage devices, reads boot sector from hard
disk, loads OS, etc...

8. OS mounts filesystems read-write.

9. Normal operation (seconds, hours, days, weeks, years...)

10. OS flushes filesystems and unmounts or remounts read-only, halts peripherals, powers off displays, etc...

11. Host DC power supplies go down if they are under CPU control;
otherwise, CPU just waits for next step.

12. AC input goes from nominal voltage to 0V (also quickly).

13. Capacitor discharge delay (minimum 20 seconds) with AC off.

14. Return to step 1.

This is a worst-case (but also very common) scenario: both the
firmware and the OS have state that can be disrupted by power failures
at inopportune times. Owners of better BIOS firmware don't have to
deal with steps 4 and 6, and owners of fully stateless computing
appliances don't have to bother with UPS monitoring at all.

When the load host has a UPS monitor daemon, it is started late in step
8. With heroic effort (a custom script early in the OS boot sequence and
waiting on a minimum level of battery charge) it can be moved to step 7.

If the UPS notifies the host about the imminent end of dwell time,
this notification occurs between step 9 and step 10. Between steps 10
and 11, the host may direct the UPS to perform steps 12-14, with step
14 to be performed only when AC power and minimum dwell time are
available. If minimum dwell time is not available, step 13 is extended
while the UPS battery charges.

At step 1 and step 12, the voltage must move quickly from zero to full
power and back. If the voltage climbs or falls slowly, the host's CPU
may get brownouts and data corruption near the critical voltage
thresholds.

If power goes down at step 2, the host may have some damaging currents
leaking out of DAC outputs due to incomplete firmware
initialization. This is mostly a concern for devices like LCD display
panels and printers which can be damaged if outputs are enabled but
clocks are not running due to low voltage. Devices like these are
supposed to have voltage-controlled output kill gates, but
manufacturers like to save money (on BoM costs if not warranty costs)
by doing it with firmware instead.

If power goes down at step 3, nothing interesting happens to the
host. We go back to step 1, good as new.

If power goes down at step 4 or step 5, the BIOS will have an error
flag recorded in CMOS. On the next boot, the BIOS will revert to
defaults, wait for a keypress, or both. This is because BIOSes now
assume that the reason they didn't get to step 6 on the previous boot
was because the configuration applied at step 5 crashed the CPU
(e.g. due to overclocking) so they should not attempt to use the same
configuration again. Oops!

If power goes down at step 7, nothing interesting happens to the host
(unless the hard drive misinterprets a read as a write or similarly
improbable phenomenon).

If power goes down between step 8 and 10, we assume assorted bad
things happen to the host (lost data and productivity). This is the
usual thing we want a UPS for.

If the power goes down at step 11, nothing interesting happens to the
host (it was prepared for that already).

If the power stays up at step 12 or step 13, the host will not reboot
at the next step 2 (the AC power level change will be missing). The
UPS has to force this by cycling power.

The main difference between a power loss at step 4/5 and a power loss
at steps 8-10 is that users can do something about the load host
behavior between steps 8 and 10 (like install a UPS monitor on their
OS to delay step 8 until the UPS battery charges). If we lose power at
step 4/5, the load host is broken until some human hand touches it.

== Requirements ==

The UPS shall not automatically send power to the load until enough
dwell time is available to get all the way from step 1 to step 11,
assuming zero time is spent at step 9 (i.e. as soon as the monitoring
software connects to the UPS, the UPS says "shut down NOW" with enough
dwell time remaining to do that). Although we can safely drop power
during steps 3 or 7, the UPS cannot determine when those steps occur
because there is no monitoring daemon to communicate with on the load
host.

If requested by the load host monitoring software, the UPS shall not
send power to the load (i.e. disable both inverter output and AC
bypass) until the minimum time required by step 13 has elapsed and
line AC power is available (note that line AC may or may not be
available when this command is issued). The load host will send such
messages to the UPS between step 10 and 11 or between 7 and 8. If the
load host sends the command at other times, the UPS will do something
bad to the load host--good load hosts shall not ask for bad
things. This is a standard UPS feature and some variant of it is
supported by many production UPSes with a monitoring port. In apcupsd,
this is the effect of the apcupsd -k command.

Minimum dwell time must be configurable by the user as only the user
can determine the worst-case time to get from step 1 to step 11 in a
specific deployment (though the UPS could measure and record actual
load host timing to help with this). Users with stateless computing
appliances don't have steps 8-10 or 4-5, so they don't need minimum
dwell at all (or a UPS monitor daemon for that matter) and can set
minimum dwell to 0.

// end
