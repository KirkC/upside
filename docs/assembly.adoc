= Assembling the UPSide-1 =

These instructions are not yet complete.

== Parts list ==

1 BeagleBone Black with USB A to USB-micro-B cable.
1 SunFounder 20x4 LCD
1 4-lead breakout cable with a female connector at one end.
1 very short piece of patch wire

== Tools ==

Light-duty soldering iron.  A hobby-grade one will do.

== Display subsystem ==

The display LCD is a Sunfounder 2004A, a 20x4 LCD with an
HD44780-compatible controller and an I2C interface.  If you know what
you're doing around this class of hardware, you could substitute any
HD44780-compatible 20x4 LCD and it will probably just work.

You'll need a breakout cable with a flat 4-wire female connector
at one end and separated male pins at the other.

With power to the BeagleBone off, wire the four pins on the right-hand
end of the little I2C daughterboard to the BeagleBone P9 connector
as follows:

[options="header"]
|====================================================
| Sunfounder | Beaglebone
| GND        | DGND (P9-1 or P9-2)
| VCC        | SYS_5V (P9-7 or P1-8)
| SDA        | I2C2_SDA (P9-20)
| SCL        | I2C2_SCL (P9-19)
|====================================================

These pins are numbered from the board corner nearest the 5V barrel
plug, odd-numbered ones nearest the board edge and even on the
inside.

Here's a pinout with the I2C pins marked in red.  You're wiring bus 2.

image::cape-headers-i2c.png[]

Power up the BB. On the SunFounder. A green power LED just below the
pin row should go on. If it doesn't, check your power and ground
placements.  The LCD should display a test pattern of two rows of
blocks.  If it doesn't, check that the green jumper connecting the
GND and VCC pins near the left edge of the board is on.

Now ssh into the BB and go root.  Run the command "i2cdetect -r 2".
You should see a display like this:

--------------------------------------------------------------
root@beaglebone:~# i2cdetect -r 2
WARNING! This program can confuse your I2C bus, cause data loss and worse!
I will probe file /dev/i2c-2 using read byte commands.
I will probe address range 0x03-0x77.
Continue? [Y/n] y
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- 27 -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- UU UU UU UU -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         
--------------------------------------------------------------

That 0x27 is the LD's default address. If you don't see that '27',
check the placement of the SDA and SCL lines. When you do see it,
the LCD is accessible on the I2C bus.

Unfortunately, we need to change that address, because it will collide
with the fixed address of another component.  To do this, find the 2x3
rectangle of through holes located just above the lower edge of the
board.  Power down everything and connect the leftmost vertical pair
with a tiny piece of wire and a little solder.

Doing this will drop the I2C bus address of the board to 26.  After
you power up and shell in your i2cdetect display should look like this:

--------------------------------------------------------------
root@beaglebone:~# i2cdetect -r 2
WARNING! This program can confuse your I2C bus, cause data loss and worse!
I will probe file /dev/i2c-2 using read byte commands.
I will probe address range 0x03-0x77.
Continue? [Y/n] y
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- 26 -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- UU UU UU UU -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         
--------------------------------------------------------------

Power everything back up.  Use "bin/remote lcdtest" to run the LCD
demo. You should see "Hello, world".

Note that at the extreme left edge of the board there is a group of
through-holes marked "I2C". When you assemble the LCD and SBC into a
full UPSide unit you will use those for the side of the I2C bus that
goes to the power plane.

Note that the blue block to the left of the I2C connector is a trim
potentiometer.  A small Phillips screwdriver inserted into the
cross-shaped slot can optionally be used to adjust the LCD contrast.

If you needed to change the I2C address to something other than 0x26,
shorting more of the vertical pairs in the 2x3 address block would
provide you with up to 3 bits of range, down to 0x20.

