#!/usr/bin/env python3

# Generate things from pseudocode describing states and actions in UPSide.
# 1. The action-state diagram for the design document
# 2. Go code for the core state machine in the policy daemon.

import sys

class dot:
    "Output class for rendering to state diagram."
    def __init__(self):
        self.nstates = 0
        self.nactions = 0
    def begin(self):
        print('strict digraph {\n    size="10,20";')
    def end(self):
        print('}')
        sys.stderr.write("%d states, %d events, %d transitions.\n" \
                % (self.nstates+1, len(events2), self.nactions+1))
    def state2(self, node, text):  ###NEW###
        print('    "{0}" [shape=oval, label="{1}"]'.format(node, text))
        self.nstates += 1
    def transition(self, fromstate, event, listOfActions, tostate, unreachable=False):  ###NEW###
        label = "Event: %s"  % (events2[ event][eventShortName])
        for action in listOfActions:
            actionStr = renderAction( action)
            if actionStr is not "":
                label += "\n" + actionStr
        arc = '    "X{2}" [shape=box, label="{3}"]; {0} -> "X{2}" -> {1}'.format(fromstate, tostate, self.nactions, label)
        if unreachable:
            arc = '    edge [style=dotted]\n' + arc + '\n    edge [style=solid]'
        print(arc)
        self.nactions += 1

class flowgraph:
    def __init__(self):
        self.states = []
        self.transitions = []
        self.captions = {}
    def state(self, node, text):
        self.states.append((node, text))
        self.captions[node] = text
    def state2(self, node, text): ### NEW ###
        self.states.append((node, text))
        self.captions[node] = text
    def transition(self, fromstate, event, listOfActions, tostate, unreachable=False):  ###NEW###
        if unreachable: return
        self.transitions.append((fromstate, event, listOfActions, tostate))
    def begin(self):
        pass

def dump_reverser(typename, keylist):
    sys.stdout.write("	reverse%s = map[%s]string{\n" % (typename, typename))
    for key in keylist:
        sys.stdout.write('		%s: "%s",\n' % (key, key))
    sys.stdout.write("	}\n")

def dump_stringer(typename):
    sys.stdout.write("""\
func (x %s) String() string {
	return reverse%s[x]
}

""" % (typename, typename))

class go_daemon(flowgraph):
    "Output class for rendering to Go source code."
    def __init__(self):
        flowgraph.__init__(self)
    def end(self):
        sys.stdout.write("""/*
 * transitions.go - policy state machine for the UPSide control daemon
 *
 * PROGRAM-GENERATED - DO NOT HAND-HACK!
 */

package main

type State int
type Event int

/*
 * Constants describing policy states.
 */
const (
""")
        for (i, (s, t)) in enumerate(self.states):
            sys.stdout.write("    %s State = %d	/* %s */\n" % (s, i, t.replace('\\n', ' ')))
        sys.stdout.write(""")

/*
 * Constants describing event types from the high-power subsystem.
 */
const (
""")

        for i in range( len(events2)):
            sys.stdout.write("    %s Event = %d	/* %s */\n" % \
                    (events2[i][eventName], i, events2[i][eventDescription]))
        sys.stdout.write(""")

/*
 * Generated Stringer methods for constants 
 */

var reverseState map[State] string
var reverseEvent map[Event] string

func init () {
""")
        dump_reverser("State", [x[0] for x in self.states])
        #dump_reverser("Event", events.keys())
        dump_reverser("Event", [events2[x][eventName] for x in events2])

        sys.stdout.write("}\n\n")

        dump_stringer("State")
        dump_stringer("Event")

        sys.stdout.write("""
var Readiness = %s

func Transition(event Event) {
\tswitch Readiness {
""" % self.states[0][0])
        for (s, t) in self.states:
            sys.stdout.write("\tcase %s:	/* %s */\n" % (s, t))
            sys.stdout.write("\t\tswitch event {\n")
            for (fromstate, event, listOfActions, tostate) in self.transitions:
                if event == EventUnreachable:
                    continue
                if fromstate == s:

                    sys.stdout.write("\t\tcase %s:\t/* %s */\n" % \
                            (events2[event][eventName], events2[event][eventDescription]))
                    sys.stdout.write("\t\t\tReportTransition(event, Readiness)\n")
                    sys.stdout.write("\t\t\tReadiness = %s;\n" % tostate)

                    for action in listOfActions:
                        sys.stdout.write("\t\t\t%s()\n" % actions[action][actionName])
            sys.stdout.write("\t\t}\n")
        sys.stdout.write("""\t}
}
""")
        sys.stdout.write("/* end */\n")


def renderAction (action):
    """ returns a string corresponding to the given action
    """
    return actions.get(action, unknown)[actionName]


    
#actions
alarmUp,\
alarmBatteryWarning,\
alarmCharging,\
alarmDown,\
alarmRestored,\
alarmShutdown,\
sayBatteryWarning,\
sayBatteryDrained,\
sayDwellLimitImminent,\
sayHostOff,\
sayUPSready,\
sayPowerOn,\
sayPowerOff,\
sayRestored,\
sayShutdown,\
cycleHostPower,\
sendHostShutdown,\
shutdown,\
outletsDisabled,\
outletsEnabled,\
unknown = range(21)

# offsets into the action tuple:
actionName = 0
actionShortDescription = 1

actions = {
    alarmUp:                ("alarmUp",                "Alarm: MAINS OK"),
    alarmBatteryWarning:    ("alarmBatteryWarning",    "Alarm: SOS"),
    alarmCharging:          ("alarmCharging",          "Alarm: CHARGING"),
    alarmDown:              ("alarmDown",              "Alarm: DOWN"),
    alarmRestored:          ("alarmRestored",          "Alarm: RESTORED"),
    alarmShutdown:          ("alarmShutdown",          "Alarm: SHUT DOWN"),

    sayBatteryWarning:      ("sayBatteryWarning",      "Battery is critical"),
    sayBatteryDrained:      ("sayBatteryDrained",      "Battery is drained"),
    sayDwellLimitImminent:  ("sayDwellLimitImminent",  "Shutdown is Imminent"),
    sayHostOff:             ("sayHostOff",             "Host is off"),
    sayUPSready:            ("sayUPSready",            "UPS is ready"),
    sayPowerOn:             ("sayPowerOn",             "Mains power is on"),
    sayPowerOff:            ("sayPowerOff",            "Mains power is off"),
    sayRestored:            ("sayRestored",            "Mains power restored"),
    sayShutdown:            ("sayShutdown",            "Shutting down now"),

    cycleHostPower:         ("cycleHostPower",         "Cycle host power"),
    sendHostShutdown:       ("sendHostShutdown",       "Send Host shutdown"),
    shutdown:               ("shutdown",               "Shutdown now"),
    outletsDisabled:        ("outletsDisabled",        "Outlets: Disabled"),
    outletsEnabled:         ("outletsEnabled",         "Outlets: Enabled"),

    unknown:                ("unknown",                "***UNKNOWN ACTION***")
}

#
# Actual policy specification starts here.  Stuff before this point was
# generic machinery to compile the specification into output products.
#

# Triggers.
"""
events = {
    "EventNone":	"No event", 
    "EventGoodAC":      "Poll of mains reports good AC voltage",
    "EventGoodBattery": "Poll reports TimeToEmpty > ShutdownTime + WarningTime",
    "EventBadAC":       "Poll of mains reports bad AC voltage",
    "EventBatteryWarning": "Poll of BMS reports Time ToEmpty <= ShutdownTime + WarningTime",
    "EventBatteryLow":  "Poll of BMS reports TimeToEmpty <= ShutdownTime",
    "EventHostOff":     "Poll of host load sensor shows current draw less than threshold",
    "EventUnreachable": "Code cannot reach this event",
    }

EventNone,         # No event
EventGoodAC,       # Poll of mains reports good AC voltage
EventGoodBattery,  # Poll reports TimeToEmpty > ShutdownTime + WarningTime
EventBadAC,        # Poll of mains reports bad AC voltage
EventBatteryWarning,  # Poll of BMS reports Time ToEmpty <= ShutdownTime + WarningTime
EventBatteryLow,   # Poll of BMS reports TimeToEmpty <= ShutdownTime
EventHostOff,      # Poll of host load sensor shows current draw less than threshold
EventUnreachable   # Code cannot reach this event
"""

EventNone,\
EventGoodAC,\
EventGoodBattery,\
EventBadAC,\
EventBatteryWarning,\
EventBatteryLow,\
EventHostOff,\
EventUnreachable = range(8) 

#index into event tuple
eventName = 0
eventShortName = 1
eventDescription = 2
events2 = {
                            # eventName,            eventShortName,   eventDescription
    EventNone:              ("EventNone",           "None",             "No event"), 
    EventGoodAC:            ("EventGoodAC",         "Good AC",          "Poll of mains reports good AC voltage"),
    EventGoodBattery:       ("EventGoodBattery",    "Good Battery",     "Poll reports TimeToEmpty > ShutdownTime + WarningTime"),
    EventBadAC:             ("EventBadAC",          "Bad AC",           "Poll of mains reports bad AC voltage"),
    EventBatteryWarning:    ("EventBatteryWarning", "Battery Warning",  "Poll of BMS reports Time ToEmpty <= ShutdownTime + WarningTime"),
    EventBatteryLow:        ("EventBatteryLow",     "Battery Low",      "Poll of BMS reports TimeToEmpty <= ShutdownTime"),
    EventHostOff:           ("EventHostOff",        "Host Off",         "Poll of host load sensor shows current draw less than threshold"),
    EventUnreachable:       ("EventUnreachable",    "Unreachable",      "Code cannot reach this event"),
    }





if __name__ == '__main__':
    import sys, re
    if len(sys.argv) ==1:
        render = dot()
    else:
        render = go_daemon()

    # This is the pseudocode describing state-action transitions.
    #
    # Actions marked "unreachable" are for the state-transition
    # diagram only, the code can never get there because they're
    # contingent on power cutting out while mains is unvailable..
    render.begin()

    render.state2("DaemonUp", "Daemon running") 
    render.transition("DaemonUp",
                       EventGoodAC, [
                       sayPowerOn,
                       alarmCharging],
                       "ChargeWait") # pushing for contants rather than strings for speed
    render.transition("DaemonUp",
                       EventBadAC, [
                       sayPowerOff,
                       alarmDown],
                       "OnBattery") # pushing for contants rather than strings for speed

    render.state2("ChargeWait", "Charge wait") 
    render.transition("ChargeWait", # was CHARGED
                       EventGoodBattery, [
                       sayUPSready,
                       outletsEnabled,
                       alarmUp],
                       "MainsUp")
    render.transition("ChargeWait", # was MAINSOFF
                       EventBadAC, [
                       sayPowerOff,
                       alarmDown],
                       "OnBattery")

    render.state2("MainsUp", "On mains power") 
    render.transition("MainsUp",
                       EventBatteryLow, [
                       alarmShutdown,
                       sayShutdown,
                       sendHostShutdown],
                       "PreShutdown")
    render.transition("MainsUp",
                       EventBadAC, [
                       sayPowerOff,
                       alarmDown],
                       "OnBattery")

    render.state2("OnBattery", "On battery power") 
    render.transition("OnBattery",
                       EventBatteryWarning, [
                       sayBatteryWarning,
                       alarmBatteryWarning],
                       "Overtime")
    render.transition("OnBattery",
                       EventGoodAC, [
                       sayRestored,
                       alarmRestored],
                       "ChargeWait")

    render.state2("Overtime", "User warned of upcoming shutdown") 
    render.transition("Overtime",
                       EventBatteryLow, [
                       sayDwellLimitImminent,
                       alarmShutdown,
                       shutdown],
                       "PreShutdown")
    render.transition("Overtime",
                       EventGoodAC, [
                       sayRestored,
                       alarmRestored],
                       "ChargeWait")

    render.state2("PreShutdown", "Awaiting power drop") 
    render.transition("PreShutdown",
                       EventGoodAC, [
                       sayRestored,
                       alarmRestored],
                       "ChargeWait")
    render.transition("PreShutdown",
                       EventHostOff, [
                       sayHostOff,
                       outletsDisabled],
                       "HostDown")
    render.transition("PreShutdown",
                       EventUnreachable, [
                       sayBatteryDrained],
                       "UPSCrash",
                       unreachable=True)

    render.state2("HostDown", "Host has shut down") 
    render.transition("HostDown",
                       EventGoodAC, [
                       sayRestored,
                       alarmUp,
                       cycleHostPower],
                       "ChargeWait")
    render.transition("HostDown",
                       EventUnreachable, [
                       sayBatteryDrained],
                       "UPSCrash",
                       unreachable=True
                       )

    render.state2("UPSCrash", "UPS goes dark") 

    render.end()

# end

