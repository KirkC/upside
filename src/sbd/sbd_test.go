/* Tests for SBD-1.1 marshalling/unmarshalling */

/*
 * This code is working but not currently used. Instead, battery state is read from 
 * /sys/class/power_supply entries.  We're holding this in reserve in
 * case we need to go to direct I2C queries.
 */

package sbd

import (
	"fmt"
	"os"
	"reflect"
	"testing"
)

var testStrings []string

func init() {
	testStrings = []string{
		"RemainingCapacityAlarm 400 mAh",
		"RemainingTimeAlarm 3 minutes",
		"BatteryMode CapacityMode InternalChargeController",
		"BatteryMode",
		"AtRate 1",
		"AtRateTimeToFull 20 minutes",
		"AtRateTimeToEmpty 20 minutes",
		"AtRateOK true",
		"AtRateOK false",
		"Temperature 273 dK",
		"Voltage 500 mV",
		"Current 30 mA",
		"AverageCurrent 22 mA",
		"MaxError 2 %",
		"RelativeStateOfCharge 75 %",
		"AbsoluteStateOfCharge 75 %",
		"RemainingCapacity 289 mAh",
		"FullChargeCapacity 10000 mAh",
		"RunTimeToEmpty 12 minutes",
		"AverageTimeToEmpty 8 minutes",
		"AverageTimeToFull 143 minutes",
		"BatteryStatus Discharging Initialized",
		"CycleCount 124 cycles",
		"DesignCapacity 1020 mAh",
		"DesignVoltage 12000 mV",
		"SpecificationInfo revision 1 version 1 vscale 1 ipscale 1",
		"ManufactureDate 2018-01-02",
		"SerialNumber 12345",
		"ManufacturerName \"Cyberdyne\"",
		"DeviceName \"My Battery\"",
		"DeviceChemistry \"Li-Ion\"",
		"ManufacturerData 232323",
	}
}

func GetBitsTest(t *testing.T, outof uint16, start uint, end uint, expect uint16) {
	check := GetBits(outof, start, end)
	if check != expect {
		t.Fatalf("in %x at (%d,%d) expected %x saw %x",
			outof, start, end, expect, check)
	}
}

func TestGetBits(t *testing.T) {
	// 0x1234 = 000100100110100
	GetBitsTest(t, 0x1234, 0, 4, 0x4)
	GetBitsTest(t, 0x1234, 15, 15, 0x0)
	GetBitsTest(t, 0x1234, 3, 6, 0x6)
}

func SetBitsTest(t *testing.T,
	check *uint16, val uint16, start uint, end uint, expect uint16) {
	SetBits(check, val, start, end)
	if *check != expect {
		t.Fatalf("setting (%d,%d) expected %x saw %x",
			start, end, expect, check)
	}
}

func TestSetBits(t *testing.T) {
	var check uint16 = 0
	SetBitsTest(t, &check, 1, 0, 1, 1)
	check = 0
	SetBitsTest(t, &check, 0x33, 14, 15, 0xC000)
	SetBitsTest(t, &check, 1, 0, 1, 0xC001)
}

func RoundTripTest(t *testing.T, text string) {
	var ctx SBDContext;
	fmt.Fprintf(os.Stderr, "Processing %v\n", text)
	msg1 := ctx.Unmarshal(text)
	fmt.Fprintf(os.Stderr, "I see %v\n", *msg1)
	dumped := ctx.Marshal(msg1)
	if text != dumped {
		t.Fatalf("round-trip failure, %q became %q\n", text, dumped)
	}
	msg2 := ctx.Unmarshal(dumped)
	if !reflect.DeepEqual(*msg1, *msg2) {
		t.Fatalf("round-trip failure, %v became %v\n", *msg1, *msg2)
	}
}

func TestRoundTrip(t *testing.T) {
	for _, v := range testStrings {
		RoundTripTest(t, v)
	}
}

// end
