// This file is Copyright (c) 2018 by the UPSide project.
// SPDX-License-Identifier: BSD-2-clause
//
package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"hardware"
)

/*
 * Display logic. Set up so the LCD driver can simply call Explanation()
 * after each poll cycle to get the string it should ship.
 */

/* Called by the actual refresh code */
var Explanation func() string

const unknown = "~"

func stringify(n int, w int) string {
	var res string
	if n == Unknown {
		res = unknown
	} else {
		res = fmt.Sprintf("%d", n)
	}
	for len(res) < w  {
		res = res + " "
	}
	return res
}

func clockify(n time.Duration) string {
	var res string
	if n == time.Duration(Unknown) {
		res = unknown
	} else {
		res = fmt.Sprintf("%v", n)
		if strings.HasSuffix(res, "ns") {
			res = "<1s"
		}
		res = "(" + res +  ")"
	}
	return res
}

func percentify(n int) string {
	var res string
	if n == Unknown {
		res = " " + unknown
	} else {
		res = fmt.Sprintf("%d%%", n)
	}
	for len(res) < 3 {
		res = res + " "
	}
	return res
}

func (ob *Observables) onMainsExplanation(status string) string {
	lv := stringify(ob.LineVoltage, 3)
	ow := stringify(ob.OutputAmps * hardware.Configuration.DCVoltage, 3)
	mf := clockify(ob.TimeToFull)
	//me := clockify(ob.TimeToEmpty)
	cp := percentify(ob.ChargePercentage)
	lp := percentify(ob.LifePercentage)
	md := clockify(ob.MaximumDwellTime)
	return fmt.Sprintf("V AC: %s Watts: %s\nCharge: %s %s\nSOB:    %s %s\nStatus: + %s",
		lv, ow, cp, mf, lp, md, status)
}

func (ob *Observables) onBatteryExplanation(status string) string {
	lv := stringify(ob.LineVoltage, 3)
	ow := stringify(ob.OutputAmps * hardware.Configuration.DCVoltage, 3)
	//mf := clockify(ob.TimeToFull)
	me := clockify(ob.TimeToEmpty)
	cp := percentify(ob.ChargePercentage)
	lp := percentify(ob.LifePercentage)
	md := clockify(ob.MaximumDwellTime)
	return fmt.Sprintf("V AC: %s Watts: %s\nCharge: %s %s\nSOB:    %s %s\nStatus: - %s",
		lv, ow, cp, me, lp, md, status)
}

func (ob *Observables) Explanation(state State) string {
	switch state {
	case DaemonUp:		/* Daemon running */
		return ob.onMainsExplanation("Startup")
	case ChargeWait:	/* Charge wait */
		return ob.onMainsExplanation("Charge Wait")
	case MainsUp:		/* On mains power */
		return ob.onMainsExplanation("On Mains")
	case OnBattery:		/* On battery power */
		return ob.onBatteryExplanation("On Battery")
	case Overtime:		/* User warned of shutdown */
		return ob.onBatteryExplanation("Overtime")
	case PreShutdown:	/* Awaiting power drop */
		return ob.onBatteryExplanation("Shutdown")
	case HostDown:		/* Host has shut down */
		return ob.onBatteryExplanation("Host Down")
	default:
		/* should never happen */
		return fmt.Sprintf("Internal error\nUnknown state %s", state)
	}
}

func Sound(alarm string) {
	if UPSide == &Simulator {
		fmt.Printf("alarm %s\n", alarm)
	}
	switch alarm {
	case "CHARGING":
		morse(".-. ") /* R */
	case "UP":
		morse("..- ") /* U */
	case "DOWN":
		morse("--. ") /* D */
	case "RESTORED":
		morse("-.- ") /* K */
	case "SOS":
		morse("...---... ") /* SOS prosign */
	case "SHUTDOWN":
		morse("--...-- ") /* BREAK prosign */
	default:
		log.Printf("Unknown alarm type %s", alarm)
	}
}

func morse(msg string) {
	const dit = 80000000   // 1 dit = 80ms
	const dah = dit * 3    // dah: 3 dits
	const sep = dit * 1    // inter-element space: 1 dit
	const spc = dit * 3    // inter-character space: 3 dits
	const brk = dit * 7    // inter-word space: 7 dits
	const freq = 440       // good frequency range: 200-600Hz
	for _, c := range msg {
		switch c {
		case '.':
			UPSide.Buzz(freq, dit)
			UPSide.Buzz(0, sep)
		case '-':
			UPSide.Buzz(freq, dah)
			UPSide.Buzz(0, sep)
		case ' ':
			UPSide.Buzz(freq, spc-sep)
		}
	}
}

func alarmUp () {
   Sound("UP")
}

func alarmBatteryWarning () {
   Sound("SOS")
}

func alarmCharging () {
   Sound("CHARGING")
}

func alarmDown () {
   Sound("DOWN")
}

func alarmRestored () {
   Sound("RESTORED")
}

func alarmShutdown () {
   Sound("SHUTDOWN")
}

func sayBatteryWarning() {
} // "Battery is critical",

func sayBatteryDrained() {
} // "Battery is drained",

func sayDwellLimitImminent() {
} // "Shutdown is Imminent",

func sayHostOff() {
} // "Host is off",

func sayUPSready() {
} // "UPS is ready",

func sayPowerOn() {
} // "Mains power is on",

func sayPowerOff() {
} // "Mains power is off",

func sayRestored() {
} // "Mains power restored",

func sayShutdown() {
} // "Shutting down now",


func cycleHostPower() {
} // "Cycle host power",

func sendHostShutdown() {
} // "Send Host shutdown",

func shutdown() {
} // "Shutdown now",

func outletsDisabled() {
} // "Outlets: Disabled",

func outletsEnabled() {
} // "Outlets: Enabled",

/* end */
