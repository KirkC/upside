// This file is Copyright (c) 2018 by the UPSide project.
// SPDX-License-Identifier: BSD-2-clause
//
// Main sequence of upsided. All the policy logic lives here.

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
	"time"
	"github.com/BurntSushi/toml"

	"hardware"
)

const version = "0.1"

/* Policy configuration */

type policyConfig struct {
	ShutdownTime time.Duration
	WarningTime time.Duration
	QuietTime time.Duration
	VoltageThreshold float32
	HostDrainThreshold int
	HostPair int
	MasterSlaves [][]string
	PowerButton uint
	TestButton uint
}

var PolicyConfiguration policyConfig

func (p *policyConfig) HostSensor() string {
	return fmt.Sprintf("sens%d", p.HostPair)
}

func (p *policyConfig) HostControl() string {
	return fmt.Sprintf("ctrl%d", p.HostPair)
}

const Unknown int = -1

func IsKnown(n int) bool {
	return n != Unknown
}

type Observables struct {
	// Don't change theses ints to uint, they need to be able to hold Unknown
	LineVoltage int
	OutputAmps int
	TimeToEmpty time.Duration
	TimeToFull time.Duration
	ChargePercentage int
	LifePercentage int
	MaximumDwellTime time.Duration
	LoadMap map[string]int
	ButtonMask int 
}

func (o *Observables) Clear() {
	o.LineVoltage = Unknown
	o.OutputAmps = Unknown
	o.TimeToEmpty = time.Duration(Unknown)
	o.TimeToFull = time.Duration(Unknown)
	o.ChargePercentage = Unknown
	o.LifePercentage = Unknown
	o.MaximumDwellTime = time.Duration(Unknown)
	o.LoadMap = make(map[string]int)
	o.ButtonMask = 0
}

func (o *Observables) CopyFrom(n *Observables) {
	o.LineVoltage = n.LineVoltage
	o.OutputAmps = n.OutputAmps
	o.TimeToEmpty = n.TimeToEmpty
	o.TimeToFull = n.TimeToFull
	o.ChargePercentage = n.ChargePercentage
	o.LifePercentage = n.LifePercentage
	o.MaximumDwellTime = n.MaximumDwellTime
	o.LoadMap = make(map[string]int)
	for k, v := range n.LoadMap {
		o.LoadMap[k] = v
	}
	o.ButtonMask = n.ButtonMask
}

func (s * Observables) ButtonGet(n uint) bool {
	return (s.ButtonMask & (1 << n)) != 0
}
var Present Observables
var Previous Observables

type PowerPlane interface {
	Init()
	Continue() bool
	SensorList() []string
	PollSensor(*hardware.SensorTake) bool
	SensorClass(string) hardware.SensorType
	Display(string)
	ShutdownHost()
	ShutdownUPS()
	OutletControl(string, bool)
	Buzz(int, time.Duration)
	Wait(time.Duration)
}

var UPSide PowerPlane = &hardware.Hardware

func init() {
	PolicyConfiguration = policyConfig{
		ShutdownTime: 30 * time.Second,	// Time required for host shutdown
		WarningTime: 60 * time.Second,	// Desired warning margin before shutdown
		HostDrainThreshold: 10,
		QuietTime: 10 * time.Millisecond,
		VoltageThreshold: 0.90,
		HostPair: 1,
		MasterSlaves: [][]string{{"sens1", "ctrl2"}},
		PowerButton: 0,
		TestButton: 1,
	}
}

/* Main sequence */

func main() {
	var showversion bool
	var simulation bool
	var writeconfig bool
	var usage bool
	configPath := flag.String("p", "/etc/upside-soft.rc",
		"read policy configuration from file")
	flag.BoolVar(&simulation, "s", false,
		"run in simulation, mode (event log on input, check to output).")
	flag.BoolVar(&writeconfig, "w", false,
		"dump the configuration in TOML mode and exit")
	flag.BoolVar(&showversion, "V", false,
		"report version and exit")
	flag.BoolVar(&usage, "?", false,
		"display this usage mesage and exit") 
	flag.Parse()

	if showversion {
		os.Exit(0)
	} else if writeconfig {
		e := toml.NewEncoder(os.Stdout)
		e.Encode(PolicyConfiguration)
		os.Exit(0)
	} else if usage {
		flag.PrintDefaults()
		os.Exit(0)
	}

	if simulation {
		UPSide = &Simulator
	}

	var err error
	conf, err := ioutil.ReadFile(*configPath)
	if err != nil {
		// Fall through silently if path nonexistent
		if os.IsExist(err) {
			log.Fatal("upsided: config file open failed: ", err)
		}
	} else {
		md, err := toml.Decode(string(conf), &PolicyConfiguration)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Errorf("upside: undecoded keys: %q\n",
			md.Undecoded())
	}

	/*
	 * According to the Smart Battery Standard, TimeEmpty measurements
	 * are allowed to be off by as much as two minutes. Easiest way to
	 * deal with this is add that margin to the shutsown rime. 
	 */
	PolicyConfiguration.ShutdownTime += time.Duration(2 * time.Minute)

	UPSide.Init()
	Present.Clear()
	Previous.Clear()
	for UPSide.Continue() {
		valid := false
		// Walk through all sensors
		Present.OutputAmps = 0
		for {
			var take hardware.SensorTake
			if !UPSide.PollSensor(&take) {
				break
			}

			if take.Type == hardware.INVALID {
				continue
			}
			takeReduce(&take, &Present)
			valid = true
		}

		if valid && !reflect.DeepEqual(Present, Previous) {
			fireEvents(&Previous, &Present)
			UPSide.Display(Present.Explanation(Readiness))
			Previous.CopyFrom(&Present)
		}
	}

	UPSide.Wait(PolicyConfiguration.QuietTime)
}

/*
 * Data reduction and policy logic starts here.
 */

func takeReduce(take *hardware.SensorTake, ob *Observables) {
	switch take.Type {
	case hardware.AC:
		if ob.OutputAmps == Unknown {
			ob.OutputAmps = 0
		}
		ob.OutputAmps += take.Amps
		ob.LoadMap[take.ID] = int(take.Amps)
	case hardware.DC:
		if strings.HasPrefix(take.ID, "mains") {
			ob.LineVoltage = int(take.Volts)
		} else {
			if ob.OutputAmps == Unknown {
				ob.OutputAmps = 0
			}
			ob.OutputAmps += take.Amps
		}
		ob.LoadMap[take.ID] = int(take.Amps)
	case hardware.BMS:
		switch take.Bms.MessageType {
		case "AverageTimeToEmpty":
			ob.TimeToEmpty = time.Duration(int(take.Bms.UnsignedIntArg))
			ob.TimeToEmpty *= time.Minute
		case "RelativeStateOfCharge":
			ob.ChargePercentage = int(take.Bms.UnsignedIntArg)
			if ob.ChargePercentage >= 100 && time.Duration(ob.TimeToEmpty) != time.Duration(Unknown) {
				ob.MaximumDwellTime = time.Duration(int(ob.TimeToEmpty))
			}
		case "AverageTimeToFull":
			ob.TimeToFull = time.Duration(int(take.Bms.UnsignedIntArg))
			ob.TimeToFull *= time.Minute
		case "AbsoluteStateOfCharge":
			ob.LifePercentage = int(take.Bms.UnsignedIntArg)
		}
	case hardware.BTN:
		ob.ButtonMask |= take.ButtonMask
	}
}

/*
 * goodVoltage tells us whether we have good voltage fron the mains
 */
func (ob *Observables) goodVoltage() bool {
	res := float32(ob.LineVoltage) >= PolicyConfiguration.VoltageThreshold * float32(hardware.Configuration.DCVoltage)
	return res
}

/*
 * BatteryEvent detects battery condition, returning it as an event that should
 * file if the condition has changed.
 */
func (seen *Observables) batteryCondition() Event {
	if seen.TimeToEmpty > PolicyConfiguration.ShutdownTime + PolicyConfiguration.WarningTime {
		return EventGoodBattery
	} else if seen.TimeToEmpty > PolicyConfiguration.ShutdownTime {
		return EventBatteryWarn
	} else {
		return EventBatteryLow
	}
}

/*
 * fireEvents works the state transitions based on comparing present and previous
 * sensor states.
 */
func fireEvents(prev *Observables, ob *Observables) {
	/* Initial enable of output when we don't have previous good voltage */
	if Readiness == DaemonUp && ob.goodVoltage() {
		Transition(EventGoodAC)
	}

	/* Transitions in mains power */
	if IsKnown(ob.LineVoltage) && IsKnown(prev.LineVoltage) {
		nv := ob.goodVoltage()
		ov := prev.goodVoltage()
		if nv && !ov {
			Transition(EventGoodAC)
		} else if !nv && ov {
			Transition(EventBadAC)
		}
	}

	/* Battery transitions */
	this := ob.batteryCondition()
	last := prev.batteryCondition()
	if this != EventNone && last != EventNone {
		Transition(this)
	}

	/* Host-down notifications */
	hostSensor := PolicyConfiguration.HostSensor()
	nv := ob.LoadMap[hostSensor]
	ov := prev.LoadMap[hostSensor]
	if ov >= PolicyConfiguration.HostDrainThreshold && nv < PolicyConfiguration.HostDrainThreshold {
		Transition(EventHostOff)
	}

	/* Do switching of slave outlets */
	for _, dep := range PolicyConfiguration.MasterSlaves {
		master := dep[0]
		oldstate := prev.LoadMap[master]
		newstate := ob.LoadMap[master]
		if oldstate != newstate {
			for _, slave := range dep[1:len(dep)] {
				UPSide.OutletControl(slave, newstate != 0)
			}
		}
	}

	/* Button interpretation */
	if ob.ButtonGet(PolicyConfiguration.PowerButton) {
		UPSide.ShutdownUPS()
	}
}

func ReportTransition(e Event, a State) {
	if UPSide == &Simulator {
		fmt.Printf("event %s -> %s\n", e, a)
	}
}

func EnableAllOutlets() {
	for _, id := range UPSide.SensorList() {
		if id == "mains" {
			continue
		}
		if UPSide.SensorClass(id) == hardware.AC || UPSide.SensorClass(id) == hardware.DC {
			UPSide.OutletControl(id, true)
		}
	}
}

func CycleHostPower() {
	host := fmt.Sprintf("ctrl%d", PolicyConfiguration.HostPair)
	UPSide.OutletControl(host, false)
	UPSide.Wait(time.Second)
	UPSide.OutletControl(host, true)
}

// end
