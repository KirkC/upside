// This file is Copyright (c) 2018 by the UPSide project.
// SPDX-License-Identifier: BSD-2-clause
//
// Encapsulates the policy logc's interface to the power plane.  It's
// mirrored by the simulator module, which allows us to test-jig the
// policy logic without talking to hardware.

package hardware

import (
	"log"
	"os/exec"
	"time"

	"golang.org/x/exp/io/i2c"

        "gobot.io/x/gobot"
        //"gobot.io/x/gobot/drivers/gpio"
        "gobot.io/x/gobot/platforms/beaglebone"

	"hd44780"
	"sbd"
)

/* Interconnect types */
type BusType int
const (
	I2C BusType = iota
	SMBUS
	GPIO
)

type SensorType int
const (
	INVALID SensorType = iota	/* so it won't show in maps */
	AC				/* AC monitor - returns voltage-current */
	DC				/* DC Monitor - returns voltage-current */
	BMS				/* Battery Management System */
	BTN				/* Button */
)

/* Encapsulate the hardware sensors */

type SensorTake struct {
	Type SensorType
	ID string
	Timestamp time.Time
	/* AC and DC load sensors */
	Volts int
	Amps int
	/* Battery management */
	Bms *sbd.SBDMessage
	/* Buttons */
	ButtonMask int
}	

func (s * SensorTake) ButtonSet(n uint, v bool) {
	s.ButtonMask |= (1 << n)
}

func (s * SensorTake) ButtonGet(n uint) bool {
	return (s.ButtonMask & (1 << n)) != 0
}

/*
 * Hardware configuration.
 *
 * Everything specific to an UPSide board variant should be described here.
 */

type deviceConfig  struct {
	Name string
	SensorType SensorType
	BusType BusType
	Bus int
	Address int
}


type HardwareConfig struct {
	DCVoltage int
	Sensors []deviceConfig
	Controls []deviceConfig
	DisplayDevice string
	DisplayAddress int
}

var UPSide1 HardwareConfig

func init() {
	UPSide1 = HardwareConfig{
		DCVoltage: 24,
		// Description of sensors taking on the I2C bus.
		// Might change in later hardware revisions.
		// FIXME: 0x0 I2C address value is a placeholder
		// FIXME: Still missing the buzzer - no idea what it'll be yet.
		Sensors: []deviceConfig{
			{"mains", AC,  I2C,   0, 0x00},	/* mains power */
			{"bms",   BMS, SMBUS, 0, 0x27},	/* smart BMS */
			{"sens0", DC,  I2C,   0, 0x00},	/* 12VDC load */
			{"sens1", AC,  I2C,   0, 0x00},	/* AC-out load */
			{"sens2", AC,  I2C,   0, 0x00},	/* AC-out load */
			{"sens3", AC,  I2C,   0, 0x00},	/* AC-out load */
			{"btn0",  BTN, GPIO,  0, 0x00},	/* UPS on/off button */
		},
		Controls: []deviceConfig{
			{"ctrl0", DC,  I2C,   0, 0x00},	/* 12VDC switch */
			{"ctrl1", AC,  I2C,   0, 0x00},	/* AC-out switch */
			{"ctrl2", AC,  I2C,   0, 0x00},	/* AC-out switch */
			{"ctrl3", AC,  I2C,   0, 0x00},	/* AC-out switch */
		},
		DisplayDevice: "/dev/i2c-2",
		DisplayAddress: 0x26,
	}
	/* There could be other variants in the future */
}	

var Configuration *HardwareConfig = &UPSide1
	
type HardwareType struct {
	sensorIndex int	/* where we are in the master sensor list */
	sensorNames []string
	adaptor gobot.Adaptor
	lcd *hd44780.Lcd
}

func (s *HardwareType) Init() {
	s.sensorNames = make([]string, 0)
	for _, v := range Configuration.Sensors {
		s.sensorNames = append(s.sensorNames, v.Name)
	}

	s.adaptor = beaglebone.NewAdaptor()

	d, err := i2c.Open(&i2c.Devfs{Dev: Configuration.DisplayDevice}, Configuration.DisplayAddress)
	if err != nil { log.Fatal(err) }
	lcd, err := hd44780.NewLcd(d, hd44780.LCD_20x4)
	if err != nil { log.Fatal(err) }
	s.lcd = lcd
	s.lcd.BacklightOn()
	s.lcd.Clear()

	log.Fatal("upside: hardware drive is not yet implemented")
}

func (s *HardwareType) Continue() bool {
	s.sensorIndex = 0
	return true
}

func (s *HardwareType) SensorList() []string {
	return s.sensorNames
}

func (s *HardwareType) PollSensor(st *SensorTake) bool {
	if s.sensorIndex > len(Configuration.Sensors) {
		return false
	}
	s.sensorIndex++

	// FIXME: Real work is done here

	return true
}

func (s *HardwareType) SensorClass(id string) SensorType {
	for _, v := range Configuration.Sensors {
		if v.Name == id {
			return v.SensorType
		}
	}
	return INVALID
}

func (s *HardwareType) Display(text string) {
	s.lcd.Display(text)
}

func (s *HardwareType) ShutdownHost() {
	// FIXME: Real work is done here
}

func (s *HardwareType) OutletControl(id string, enable bool) {
	// FIXME: Real work is done here
}

func (s *HardwareType) Buzz(freq int, dur time.Duration) {
	// FIXME: Real work is done here
}

func (s *HardwareType) Wait(t time.Duration) {
	time.Sleep(t)
}

func (s *HardwareType) ShutdownUPS() {
	exec.Command("shutdown -h now").Run()
	log.Fatal("shutdown command failed")
}

var Hardware HardwareType

// end



